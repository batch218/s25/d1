// console.log("Hello wolrd");

//[SECTION] JSON Objects
/*
  -JSON stands for Javascript Object Notation
  - A common use of JSON is to read data from a web server, and display the data in a web page
  - Features of JSON:
        - It is lighweight data-interchange format
        - It is easy to read and write 
        - It is easy for machines to parse and generate
*/

//JSON Objects
/*
   - JSON also use the "key/value pairs" just like the object properties in Javascript
   - "Key/property" names requires to be enclosed with double quotes.

	      Syntax:
	      {
		      "propertyA" : "valueA",
		      " propertyB" : "value"
	      }

*/

//Example of JSON objects
// {
//   "city" : "Quezon City",
//   "province": "Metro Manila",
//   "country" : "Philippines"
// }

//Arrays of JSON object
/*
'cities': [
  {
    "city" : "Quezon City"
    "Province": "Metro Manila"
    "country" : "Philippines"
  }
  {
    "city" : "Makati City"
    "Province": "Metro Manila"
    "country" : "Philippines"
  }
]
*/
console.log("-----------------------------");
// Javascript Array of Objects
let batchArr = [
  {
    batch: "Batch218",
    schedule: "Part Time"
  },
  {
    batch: "Batch219",
    schedule: "Full Time"
  }
]

console.log(batchArr);

// The "stringify" method is used to convert javascript objects into a string 
console.log("Result of stringify method");
console.log(JSON.stringify(batchArr));
 

let data = JSON.stringify({
  name: "John",
  age: 31,
  address:{
    city: "Manila",
    country:"Philippines"
  }
});
console.log(data);
// //Global variables
// // let firstName = prompt("Enter your first name: ");
// // let lastName = prompt("Enter your last name: ");
// // let email = prompt("Enter your email: ");
// // let password = prompt("Enter your password: ");

// let otherData = JSON.stringify({
//   firstName: firstName,
//   lastName:  lastName,
//   email:email,
//   password:password
// });
// console.log(otherData);

//[SECTION] Converting Stringified JSON into Javascript objects
//Javascript 

let batchesJSON = `[
  {
    "batchName": "Batch218",
    "schedule" : "Part time"

  },
  {
    "batchName": "Batch219",
    "schedule" : "Full time"

  }
]`
console.log("batchesJSON content: ")
console.log(batchesJSON);
//JSON parse method converts JSON Object into javascript Object
console.log("Result From parse method: ")
// console.log(JSON.parse(batchesJSON));


console.log("-----------------------------");
let parseBatches = JSON.parse(batchesJSON);
console.log(parseBatches);
console.log("Accessing the array value: ")
console.log(parseBatches[0].batchName);

console.log("-----------------------------");
let stringifiedObject = `{
  "name" : "John",
  "age" : 31,
  "address" :{
    "city" : "Manila",
    "country" : "Philippines"
  }
}`
console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject));